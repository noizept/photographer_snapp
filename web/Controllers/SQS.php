<?php

/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 29-07-2015
 * Time: 16:54
 */
require '../../vendor/autoload.php';
require '../Global-Defines/AWS-define.php';
use Aws\Sqs\SqsClient;

class SQS_Service
{
    /***
     * @var static Stores the SQS client
     */
    private $mSqs;

    /***
     * Connects to the SQS service and stores the client on $mSqs
     */
    function __construct(){
        $this->mSqs=SqsClient::factory([
            'region'=>'us-west-2',
            'credentials' => array(
                                'key'    => AWS_ID,
                                'secret' => AWS_SECRET,
                                 ),
            'version'=>'latest'

        ]);
    }

    /***
     * Sends message to the selected Queue Url
     * @param $aQueueUrl The Queue URL of the amazon SQS
     * @param $aMessage Message to be sent to SQS
     */
    public function sendMessage($aQueueUrl,$aMessage){
        $this->mSqs->sendMessage(array(
            'QueueUrl'    => $aQueueUrl,
            'MessageBody' => $aMessage,
        ));
    }

    /***
     * Returns the Array with the SQS messages
     * @param $aQueueUrl
     * @return mixed
     */
    public function getMessages($aQueueUrl){
        return $this->mSqs->receiveMessage(array(
            'QueueUrl' => $aQueueUrl,
            'MaxNumberOfMessages'=>10
        ));
    }

}