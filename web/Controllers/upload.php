<?
    require "AWS.php";
    require "DB.php";
    require "functions.php";
    require "SQS.php";
    $db=new DB();
    $aws=new AWService();
    $sqs=new SQS_Service();

    /** @var  $exif  Reads the metadata on the photo*/
    $exif=exif_read_data($_FILES['file']['tmp_name']);
    /** @var  $latitude checks if there is metadata and stores that information */
    if($latitude = gps($exif["GPSLatitude"], $exif['GPSLatitudeRef'])){
        $longitude = gps($exif["GPSLongitude"], $exif['GPSLongitudeRef']);
        $timetaken = time_conv($exif);
    }
    else {
        $latitude = null;
        $longitude = null;
        $timetaken = null;
    }



    /** @var  $url uploads the file to AWS */
    $url=$aws->uploadtoaws($_FILES,'testInt');

    $photo[]=array('photographer_email'=>'No_PHOTOGRAPHER_YET','latitude'=>$latitude,'longitude'=>$longitude,
                    'message'=>'empty','link'=>$url,'time'=>$timetaken,'status_code'=>0);
    $sqs->sendMessage(QUEUE_URL,json_encode($photo));


    /**  Inserts the data in the database */
//    $db->insertPhoto('No_PHOTOGRAPHER_YET',$latitude,$longitude,'empty',$url,$timetaken,0);






