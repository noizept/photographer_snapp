<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 22-07-2015
 * Time: 18:39
 *
 */
    require "DB.php";
    require "AWS.php";
    require "functions.php";
    $db=new DB();

    /** Checks if the photo is a filtered one, containing 'feather-cliente-files' in the link and if not keeps it blank */
    if(!strpos($_POST['newPhoto'],'feather-client-files')){
        $url="";
    }
    else {
        /** gets the new filtered photo and uploads to the AWS */
        $aws=new AWService();
        $url=$aws->getFilteredPhoto('testInt',$_POST['newPhoto']);
    }
    $time="";
    empty($_POST['photoTime'])?$time="":$time=timepicker_to_dbtime($_POST['photoTime']);

    echo $db->updatePhoto($_POST['photoID'],$_POST['photographer'],$_POST['Latitude'],$_POST['Longitude'],$_POST['message'],$time,$_POST['aceptance'],$url);
