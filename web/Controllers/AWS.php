<?php
require '../../vendor/autoload.php';
require '../Global-Defines/AWS-define.php';
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

class AWService
{
    /**
     * @var static
     * Stores the S3Client variable, so we can make calls to the API latter on
     * by calling this
     */
    private $client;


    /**
     * Construtor override
     * On initiate it will set $client variable to the Factory method by the API
     */
    function __construct(){
        $this->client=S3Client::factory([
            'region'=>'us-west-2',
            'version'=>'latest'
        ]);
    }

    /**
     * @param $FILE the $_FILE object from the HTML Form
     *
     * @return URL
     */
    public  function uploadtoaws($FILE,$user_email){
        $file=$FILE['file'];

        //File Details
        $name=$file['name'];
        $tmp_name=$file['tmp_name'];
        $extension= explode('.',$name);
        $extension=strtolower(end($extension));

        //temp details
        $key=md5(uniqid());
        $tmp_file_name= "{$key}.{$extension}";
        $tmp_file_path=$_SERVER['DOCUMENT_ROOT']."/uploads/{$tmp_file_name}";

        //move file
        move_uploaded_file($tmp_name,$tmp_file_path);

        try{
            $url=$this->__upload($user_email,$tmp_file_name,$tmp_file_path);

        }catch (S3Exception $e){
            die($e->getMessage());
        }
        return $url;

    }

    /**
     * @param $aUser_email
     * @param $aName The name that file will have once its uploaded
     * @param $aTmp_file_path The directory of the file that we are going to upload
     * @return URL of the uploaded file
     */
    private function __upload($aUser_email,$aName,$aTmp_file_path){
       $upload= $this->client->upload(S3_BUCKET,"{$aUser_email}/{$aName}",fopen($aTmp_file_path,'rb'),'public-read');
        unlink($aTmp_file_path);
        return $upload->get('ObjectURL');

    }

    /***
     * Transfers file from a certain link to our AWS
     * @param $folder of destination in our AWS
     * @param $file the link of the photo
     * @return the link of the photo
     */
    public function getFilteredPhoto($folder,$file){
        $photoname=explode('/',$file);
        $photoname=strtolower(end($photoname));
        $photoname='_filter_'.$photoname;

        $upload = $this->client->upload(S3_BUCKET,"{$folder}/{$photoname}",fopen($file,'rb'),'public-read');
        return $upload->get('ObjectURL');
    }


}