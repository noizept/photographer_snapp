<?php


require "../../vendor/autoload.php";
require "../Global-Defines/DB-define.php";

class DB
{
    /***
     * @var Keeps the connection to the database
     */
    private $connection;

    /***
     * Override constructor, and sets the $connection
     */
    function __construct(){
        $this->startConnection();
    }


    /***
     * Establish connection to the database and stores this instance
     * on the $connection variable
     * @return null if fails to connect
     */
    private   function startConnection()
    {
            try{
                $this->connection= new PDO("pgsql:host=".DB_URL.";
                port=".DB_PORT.";
                dbname=".DB_NAME.";
                user=".DB_USER.";
                password=".DB_PASSWORD);
                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            }
            catch(\Exception $e){
                echo $e->getMessage();
                return null;
            }
    }

    /***
     * @return array Returns the list of photographers on the photographer table
     */
    public function getPhotoGraphers(){
        $sql='SELECT * from photographer';
        $sthm=$this->connection->prepare($sql);
        $sthm->execute();
        $photog=array();
        while ($row = $sthm->fetch(PDO::FETCH_ASSOC)) {
            array_push($photog,$row['email']);
        }
        return $photog;
    }

    /**
     * @param $sql recieves a SQL paramenter and tries to execute the query against the Database
     * @return bool if it fails returns False
     */
    public function execute($sql){
        try {
            $this->connection->exec($sql);
            echo "got here before excp";
        } catch(PDOException $e) {
            echo "got esception";
            echo $e->getMessage();
            return false;
        }
    }


    /***
     * Inserts a newly uploaded photo in the database
     * @param $photographer_email
     * @param $latitude
     * @param $longitude
     * @param $message
     * @param $link
     * @param $time
     * @param $status_code
     * @return bool
     */
    public function insertPhoto($photographer_email, $latitude, $longitude, $message, $link, $time, $status_code)
    {
        try {
            $statement = $this->connection->prepare("INSERT INTO photos(photographer_email,latitude,longitude,message,link,time,status_code) VALUES(?, ?, ?, ?, ?, ?, ?)");
            $statement->execute(array($photographer_email, $latitude, $longitude, $message, $link, $time, $status_code));
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Queries the database for all posts and returns the query result
     * @return PDO -fetch
     */
    public function getAllrows(){
        $sql='SELECT * FROM photos';
        $sql.=' WHERE status_code=0';
        $sql.=' LIMIT 6';

        $sthm=$this->connection->prepare($sql);
        $sthm->execute();
        return $sthm;
    }

    public function getPhotoId($aId){
        try {
            $sql = 'SELECT * FROM photos';
            $sql .= " WHERE photo_id='{$aId}'";
            $sthm = $this->connection->prepare($sql);
            $sthm->execute();
            return $sthm;
        }catch (Exception $e){
            return false;
        }

    }

    /***
     * Counts the number of photos still to be acepted and prints it
     * @return string|void
     */
    public function countPhotos(){
        $sql="SELECT COUNT(*) FROM photos where status_code='0'";
        try {
            $sthm = $this->connection->prepare($sql);
            $sthm->execute();
        }catch (Exception $e){
            return  $e->getMessage();
        }
        while ($row = $sthm->fetch(PDO::FETCH_ASSOC)) {
            if(!$row['count']) die('NO MORE IMAGES AVAILABLE');
            else echo "<p align='center'>There is still {$row['count']} Photos Available</p>";
            return;
        }


    }

    /***
     * Update the Photo_id record with the new fields once its acepted or rejected
     * @param $photo_id
     * @param $photographer_email
     * @param $latitude
     * @param $longitude
     * @param $message
     * @param $time
     * @param $status_code
     * @param $photofilter
     * @return string
     */
    public function updatePhoto($photo_id,$photographer_email,$latitude,$longitude,$message,$time,$status_code,$photofilter){

        $sql="UPDATE photos ";
        $sql.="SET status_code={$status_code},photographer_email='{$photographer_email}',latitude='{$latitude}',
        longitude='{$longitude}',message='{$message}',photo_filter='{$photofilter}'";
        if(!empty($time)) $sql.=",time='{$time}' ";

        $sql.="  WHERE photo_id='".$photo_id."'";
        try {
            $sthm = $this->connection->prepare($sql);
            $sthm->execute();
        }catch (Exception $e){
            echo $e->getMessage();
        }
        return $sql;
    }
}