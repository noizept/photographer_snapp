<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 7/18/15
 * Time: 4:29 AM
 */

/**
 * @param $exif exif array from exif_data_read
 * @return array|string returns returns the string converted from yyyy:mmm:...... to yyyy-mm-.......
 */
function time_conv($exif){
    $extension= explode(' ',$exif['DateTime']);
    $extension[0]=str_replace(':','-',$extension[0]);
    $extension=join(' ',$extension);
    return $extension;
}

/**
 * Returns the converted GPS coordinated
 * @param $coordinate
 * @param $hemisphere
 * @return int
 */
function gps($coordinate, $hemisphere) {
    for ($i = 0; $i < 3; $i++) {
        $part = explode('/', $coordinate[$i]);
        if (count($part) == 1) {
            $coordinate[$i] = $part[0];
        } else if (count($part) == 2) {
            $coordinate[$i] = floatval($part[0])/floatval($part[1]);
        } else {
            $coordinate[$i] = 0;
        }
    }
    list($degrees, $minutes, $seconds) = $coordinate;
    $sign = ($hemisphere == 'W' || $hemisphere == 'S') ? -1 : 1;
    return $sign * ($degrees + $minutes/60 + $seconds/3600);
}


/***
 * Verifies and Edits the time-date format so it's acepted by the database
 * @param $aTime Time string either from the photo or selected by user
 * @return array|string
 */
    function timepicker_to_dbtime($aTime){
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/", $aTime)){
            return $aTime;
        }
        $time=explode(' ',$aTime);
        $time[0] =str_replace('/','-',$time[0]);
        $time=join(' ',$time);
        $time.=':00';
        return $time;
    }

