<?php

/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 24-07-2015
 * Time: 16:47
 */
 /***
* Class FormFiller
 * Class responsible for displaying the data to the the user (the form / photo )
 */
class FormFiller
{

    /***
    * @var  Array containing the Photographers
    */
    private $photograhpers;

    /***
    * @var The Query result from the database
    */
    private $query;
    function __construct($photographers,$query){
        $this->photograhpers=$photographers;
        $this->query=$query;
    }


    /***
    * Fills the Page with the Form/image and javascript on the buttons (this is considered the View
    */
    public function fillTable()
    {
        $photographers = $this->photograhpers;
        $query = $this->query;
        ?>
        <div id="content">
            <ul id="picturelist">
        <?php

                while ($row = $query->fetch(PDO::FETCH_ASSOC)) { ?>
                  <li id="div<?php echo $row['photo_id']; ?>">
                    <div  align="center" style="margin-bottom: 6px;">
                         <button class ="remoteButton" id="but<?php echo $row['photo_id']; ?>">Remove</button>
                         <button class ="editButton" id="edit<?php echo $row['photo_id']; ?>">Edit Photo</button>

                    </div>

                <img width="500" height="340" id="photo<?php echo $row['photo_id']; ?>" src="<?php echo  $row['link'];?>"/>
                <form style="margin-top: 6px;" action="../Controllers/ajaxPhotoHandle.php" method="post" id="aForm<?php echo $row['photo_id']; ?>">
                    Latitude :
                    <input name="Latitude" type="text" style="width:90px;" value="<?php echo  $row['latitude'];?>">
                    Longitude :
                    <input name="Longitude" type="text" style="width:90px;" value="<?php echo  $row['longitude'];?>">
                    Time :
                    <input id="phototimer<?php echo $row['photo_id']; ?>" type="text" name="photoTime" style="width:125px;" value="<?php echo  $row['time'];?>">
                    <br> Message :
                    <textarea style="margin-top: 10px;"  name="message" rows="1" cols="50"></textarea>
                    <br> PhotoGraher :<select name="photographer">
                                            <?php
                                            foreach ($photographers as $phore) {
                                                echo '<option value="'.$phore.'"> '. $phore .'</option>';
                                            }?>
                                    </select>
                    <input id="aceptance<?php echo $row['photo_id']; ?>" name="aceptance" type="hidden" value="1">

                    <input type="hidden" name="photoID" value="<?php echo $row['photo_id']; ?>">
                    <input type="hidden" name="newPhoto" value="">
                    <input type="hidden" name="photoidelement" value="photo<?php echo $row['photo_id']; ?>">

                </form>
              </li>
                    <!-- Jquery function for button click !-->
            <script>
                $("#edit<?php echo $row['photo_id']; ?>").on('click',function(){
                   launchEditor('photo<?php echo $row['photo_id']; ?>','<?php echo  $row['link'];?>');

                });

                $("#but<?php echo $row['photo_id']; ?>").on('click',function(){
                    alertify.confirm("Are you sure you want to delete?", function (e) {
                        if (e){
                            // user clicked "ok"
                            $('#aceptance<?php echo $row['photo_id']; ?>').val('2');
                            $('#div<?php echo $row['photo_id']; ?>').foggy();
                           	$("#but<?php echo $row['photo_id']; ?>").attr("disabled", true);
                           	$("#edit<?php echo $row['photo_id']; ?>").attr("disabled", true);

                        }
                    });
                });
                $("#phototimer<?php echo $row['photo_id']; ?>").datetimepicker({
                    formatTime:"H:i",
                    formatDate:"Y-m-d"
                })
            </script>


            <?php
        }
              echo'</ul>';
        echo '</div>';
    }
}