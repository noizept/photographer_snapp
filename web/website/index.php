<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>

<body>
<script type="text/javascript" src="../js/jquery.js"></script>
<script src="../js/dropzone.js"></script>
<link rel="stylesheet" type="text/css" href="../css/dropzone.css">


<div id="dZUpload" class="dropzone">
      <div class="dz-default dz-message"></div>
</div>

<form action="validationPage.php" method="post">
    <button type="submit">Done</button>
</form>


</body>
<script type="text/javascript">
    $(document).ready(function () {
    Dropzone.autoDiscover = false;
    $("#dZUpload").dropzone({
        url: "../Controllers/upload.php",
        addRemoveLinks: false,
        maxFilesize: 40,
        success: function (file, response) {
            var imgName = response;
            file.previewElement.classList.add("dz-success");
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        }
    });
});
</script>
</html>