<!DOCTYPE html>
<html lang="en">

<script src="../js/jquery.js"></script>
<script src="../js/alertify.js"></script>
<script src="../js/foggy.js"></script>
<script src="../js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" href="../js/alertify.core.css" />
<link rel="stylesheet" href="../js/alertify.default.css" />
<link rel="stylesheet" href="../css/buttons.css" />
<link rel="stylesheet" href="../css/main.css" />
<link rel="stylesheet" href="../css/jquery.datetimepicker.css" />

<!-- Load Feather code -->
<script type="text/javascript" src="../js/http_feather.aviary.com_imaging_v2_editor.js"></script>
<!-- Instantiate Feather -->
<script type='text/javascript'>
    var featherEditor = new Aviary.Feather({
        apiKey: 'eaf9d28e12384e29b51908a9cdccfcdd',
        theme: 'dark', // Check out our new 'light' and 'dark' themes!
        tools: 'all',
        appendTo: '',
        onSave: function(imageID, newURL) {
            var img = document.getElementById(imageID);
            img.src = newURL;
        },
        onError: function(errorObj) {
            alert(errorObj.message);
        }
    });
    function launchEditor(id, src) {
        featherEditor.launch({
            image: id,
            url: src
        });
        return false;
    }
</script>


<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<?php
    require "../Controllers/DB.php";
    require "../Controllers/functions.php";
    require "../Controllers/FormFiller2.php";
    $db=new DB();
    $db->countPhotos();
    $filler=new FormFiller($db->getPhotoGraphers(),$db->getAllrows());
    $filler->fillTable();
?>

    <div align="center">
        <button class="btnaceptall"> Acept all </button>
    </div>
    <script>
        $('.btnaceptall').on('click',function(){
            $('form').each(function(){
                $( this).submit(function(e){
                    var photoForForm=$("input[name='photoidelement']" , "#"+this.id).val();
                    $("input[name='newPhoto']" , "#"+this.id).val($("#"+photoForForm).attr('src'));
                    var postData = $(this).serializeArray();
                    var formURL = $(this).attr("action");
                    $.ajax(
                        {
                            url : formURL,
                            type: "POST",
                            data : postData,
                            success: function(data, textStatus, jqXHR)
                            {

                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {
                            }
                        });

                    e.preventDefault(); //STOP default action
                });

                $(this).submit();

            });
            setTimeout(function(){ console.log('sleeping'); },2000);
            window.location.reload();
        });

    </script>

</body>
</html>