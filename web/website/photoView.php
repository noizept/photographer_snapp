<?php
    require "../Controllers/DB.php";
    if(empty($_GET['pid'])) die('oopps something went wrong');
    $db = new DB();
    if(empty($photo=$db->getPhotoId($_GET['pid']))) die ('ooops something went wrong');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Welcome!</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/photoview.css">
</head>
<body>
<?php while ($row = $photo->fetch(PDO::FETCH_ASSOC)) { ?>

<div id="photoViewer-divContainer">
    <div id="photoViewer-photo">
        <img src="<?php echo $row['link']; ?>" style="width: inherit; height: inherit">
    </div>

    <div id="photoViewer-information">
        <div align="center">
            <b>Photo taken by:</b>
            <br>
            <i class="material-icons md-18">face</i> <?php echo $row['photographer_email']; ?>
        </div>
        <br>

        <div align="center">
            <b>On date:</b>
            <br>
            <i class="material-icons md-18">event_note</i> <?php echo $row['time']; ?>
        </div>
        <br>

        <div align="center">
            <b>Peek the location:</b>
            <br>
            <i style="margin-top: 6px" class="material-icons md-18">room</i>
            <a href="http://google.com/maps/?q=<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>" target="_blank">View on map</a>
        </div>
        <br>

        <div align="center">
            <b>Note:</b>
            <br>
        </div>
        <i class="material-icons md-18" style="margin-right: 10px">message</i>
        <div style="width: 350px"> <?php echo $row['message']; ?></div>
    </div>
</div>
<?php } ?>
</body>
</html>
